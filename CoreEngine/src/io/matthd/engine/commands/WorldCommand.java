package io.matthd.engine.commands;

import io.matthd.core.Core;
import io.matthd.core.backend.player.CorePlayer;
import io.matthd.core.backend.player.CorePlayerManager;
import io.matthd.engine.Engine;
import io.matthd.engine.maps.EngineMap;
import io.matthd.engine.maps.MapManager;
import io.matthd.engine.maps.Spawn;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import sun.misc.MessageUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by Matthew on 2016-01-03.
 */
public class WorldCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(cmd.getName().equalsIgnoreCase("setup")){
            if(!(sender instanceof Player)){
                return true;
            }

            Player pl = (Player) sender;
            CorePlayer cplayer = Core.getInstance().getPlayerManager().getCorePlayer(pl.getUniqueId());


            if(!Core.getInstance().getPlayerManager().isAuthorized("admin", cplayer)){
                return true;
            }

            if(args.length == 0){
                World world = cplayer.getBukkitPlayer().getWorld();
                File wfile = new File(world.getWorldFolder(), "engine.yml");

                if(wfile.exists()){
                    cplayer.sendMessage("There is already world data for this world! Try /setup help");
                    return true;
                }

                try {
                    wfile.createNewFile();
                    cplayer.sendMessage("Created a empty data folder in: " + wfile.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if(args[0].equalsIgnoreCase("addspawn")){
                World world = pl.getWorld();
                EngineMap map = Engine.getInstance().getCurrentGame().getManager(MapManager.class).get(world);
                if(map == null){
                    cplayer.sendMessage("&cThe map is not setup! Maybe the world is incorrect?");
                    return true;
                }

                Spawn spawn = new Spawn(map.getSpawns().size()+1, pl.getLocation());
                map.addSpawn(spawn);
                cplayer.sendMessage("&aAdded a new spawn to the map: " + map.getName());
                return true;
            }

            if(args[0].equalsIgnoreCase("author")){

                if(args.length != 2){
                    return true;
                }

                World world = pl.getWorld();
                EngineMap map = Engine.getInstance().getCurrentGame().getManager(MapManager.class).get(world);
                if(map == null){
                    cplayer.sendMessage("&cThe map is not setup! Maybe the world is incorrect?");
                    return true;
                }

                map.setAuthor(args[1]);
                cplayer.sendMessage("&aSet the author too: " + args[1]);
            }
            else if(args[0].equalsIgnoreCase("name")){
                if(args.length != 2){
                    return true;
                }

                World world = pl.getWorld();
                EngineMap map = Engine.getInstance().getCurrentGame().getManager(MapManager.class).get(world);
                if(map == null){
                    cplayer.sendMessage("&cThe map is not setup! Maybe the world is incorrect?");
                    return true;
                }

                map.setName(args[1]);
                cplayer.sendMessage("&aSet the map name too: " + args[1]);
            }
        }

        return false;
    }
}
