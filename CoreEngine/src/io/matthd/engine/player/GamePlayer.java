package io.matthd.engine.player;

import io.matthd.core.Core;
import io.matthd.core.backend.player.CorePlayer;
import io.matthd.core.backend.rank.Rank;
import io.matthd.engine.Engine;
import io.matthd.engine.game.Game;
import org.bukkit.entity.Player;

/**
 * Created by Matthew on 2016-01-02.
 */
public class GamePlayer extends CorePlayer {

    private Engine engine = Engine.getInstance();

    private PlayerState playerState = PlayerState.UNKNOWN;
    private Game game;

    public GamePlayer(Player player) {
        super(player);
        this.game = engine.getCurrentGame();
    }
}
