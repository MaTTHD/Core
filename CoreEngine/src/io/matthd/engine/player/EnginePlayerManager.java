package io.matthd.engine.player;

import io.matthd.core.backend.player.CorePlayer;
import io.matthd.core.backend.player.CorePlayerManager;
import io.matthd.engine.Manager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthew on 2016-01-02.
 */
public class EnginePlayerManager extends CorePlayerManager implements Manager {

    private List<GamePlayer> players = new ArrayList<>();

    public GamePlayer getGamePlayer(CorePlayer player) {
        for(GamePlayer gp : players){
            if(gp.getName().equalsIgnoreCase(player.getName())){
                return gp;
            }
        }
        return null;
    }
}
