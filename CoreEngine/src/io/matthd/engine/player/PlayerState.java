package io.matthd.engine.player;

/**
 * Created by Matthew on 2016-01-02.
 */
public enum PlayerState {

    SPECTATOR, QUIT, INGAME, LOBBY, UNKNOWN;
}
