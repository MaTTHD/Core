package io.matthd.engine.maps;

import org.bukkit.Location;

/**
 * Created by Matthew on 2016-01-03.
 */
public class Spawn {

    private int id;
    private Location location;

    public Spawn(int id, Location location){
        this.id = id;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
