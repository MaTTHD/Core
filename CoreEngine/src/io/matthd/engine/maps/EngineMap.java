package io.matthd.engine.maps;

import io.matthd.engine.Engine;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthew on 2016-01-02.
 */
public class EngineMap {

    private String name;
    private String author;
    private List<Spawn> spawns = new ArrayList<>();
    private World world;

    public EngineMap(World world) throws FileNotFoundException {
        File worldData = new File(world.getWorldFolder(), "engine.yml");
        if(!worldData.exists()) {
            throw new FileNotFoundException("The file " + world.getWorldFolder().getName() + "/engine.yml does not exist, /engine init to create a empty file! ");
        }
        this.world = world;

        FileConfiguration config = YamlConfiguration.loadConfiguration(worldData);
        List<String> cs = config.getStringList("spawns");

        int id = 0;

        for(String spawn : cs){
            String[] split = spawn.split(",");
            Location loc = new Location(world, Double.valueOf(split[1]), Double.valueOf(split[2]), Double.valueOf(split[3]), Float.valueOf(split[4]), Float.valueOf(split[5]));
            spawns.add(new Spawn(id++, loc));
        }
        author = config.getString("author");
        name = config.getString("name");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        File dataFile = new File(world.getWorldFolder(), "engine.yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(dataFile);
        config.set("name", name);
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        File dataFile = new File(world.getWorldFolder(), "engine.yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(dataFile);
        config.set("author", author);
        this.author = author;
    }

    public List<Spawn> getSpawns() {
        return spawns;
    }

    public void setSpawns(List<Spawn> spawns) {
        this.spawns = spawns;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public void addSpawn(Spawn spawn){
        File dataFile = new File(world.getWorldFolder(), "engine.yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(dataFile);
        config.getStringList("spawns").add(world.getName() + "," + spawn.getLocation().getX() + "," + spawn.getLocation().getY() + "," + spawn.getLocation().getZ() + "," + spawn.getLocation().getYaw() + "," + spawn.getLocation().getPitch());
        spawns.add(spawn);
    }
}
