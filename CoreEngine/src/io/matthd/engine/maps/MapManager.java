package io.matthd.engine.maps;

import io.matthd.core.gameapi.game.GameMap;
import io.matthd.engine.Manager;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthew on 2016-01-03.
 */
public class MapManager implements Manager {

    public List<EngineMap> allMaps = new ArrayList<>();

    public List<EngineMap> getAllMaps() {
        return allMaps;
    }

    public EngineMap get(World world){
        for(EngineMap map : allMaps){
            if(map.getWorld().getName().equalsIgnoreCase(world.getName())){
                return map;
            }
            else try {
                return new EngineMap(world);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void loadAll(){
        allMaps.clear();
        for(World world : Bukkit.getWorlds()){
            try {
                allMaps.add(new EngineMap(world));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
