package io.matthd.engine.events;

import io.matthd.engine.game.GameState;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by Matthew on 2016-01-04.
 */
public class EngineStateChangeEvent extends Event {

    private GameState old, current;

    public EngineStateChangeEvent(GameState old, GameState current){
        this.old =old;
        this.current = current;
    }

    public GameState getOld() {
        return old;
    }

    public GameState getCurrent() {
        return current;
    }

    @Override
    public HandlerList getHandlers() {
        return null;
    }
}
