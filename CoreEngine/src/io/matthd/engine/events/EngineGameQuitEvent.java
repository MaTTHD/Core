package io.matthd.engine.events;

import io.matthd.engine.Engine;
import io.matthd.engine.game.GameState;
import io.matthd.engine.player.GamePlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by Matthew on 2016-01-04.
 */
public class EngineGameQuitEvent extends Event {


    private GamePlayer player;
    private GameState state;

    public void EngineGameJoinEvent(GamePlayer player){
        this.player = player;
        this.state = Engine.getInstance().getCurrentGame().getState();
    }

    public GamePlayer getPlayer() {
        return player;
    }

    @Override
    public HandlerList getHandlers() {
        return null;
    }
}
