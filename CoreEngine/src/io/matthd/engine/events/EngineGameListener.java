package io.matthd.engine.events;

/**
 * Created by Matthew on 2016-01-04.
 */
public abstract class EngineGameListener {

    public abstract void onJoin(EngineGameJoinEvent e);
    public abstract void onQuit(EngineGameQuitEvent e);
    public abstract void onStateChange(EngineStateChangeEvent e);
    public abstract void onTick(EngineTickEvent e);
}
