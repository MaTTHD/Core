package io.matthd.engine.game;

import io.matthd.core.modular.Module;

/**
 * Created by Matt on 05/01/2016.
 */
public abstract class GameProvider extends Module {

    public abstract Game create();
}
