package io.matthd.engine.game;

/**
 * Created by Matthew on 2016-01-02.
 */
public enum GameState {

    WAITING,STARTED,FULL;
}
