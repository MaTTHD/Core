package io.matthd.engine.game;


import io.matthd.engine.CountManager;
import io.matthd.engine.Manager;
import io.matthd.engine.events.EngineGameJoinEvent;
import io.matthd.engine.events.EngineGameListener;
import io.matthd.engine.events.EngineGameQuitEvent;
import io.matthd.engine.maps.MapManager;
import io.matthd.engine.player.EnginePlayerManager;
import io.matthd.engine.player.GamePlayer;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Matthew on 2016-01-02.
 */
public class Game {

    private List<Manager> managers = new ArrayList<>();

    private GameState state;
    private EngineGameListener gameListener;

    public Game(){
        Manager[] managers = new Manager[] {new CountManager(), new EnginePlayerManager(), new MapManager()};
        this.managers.addAll(Arrays.asList(managers));
    }

    public void join(GamePlayer player){
        Bukkit.getServer().getPluginManager().callEvent(new EngineGameJoinEvent());
    }

    public void leave(GamePlayer player){
        Bukkit.getServer().getPluginManager().callEvent(new EngineGameQuitEvent());
    }

    public <T extends Manager> T getManager(Class<T> clazz){
        if(clazz == Manager.class){
            return null;
        }
        for(Manager m : managers){
            if(m.getClass().isAssignableFrom(clazz)){
                return (T) m;
            }
        }
        return null;
    }

    public void addManager(Manager manager){
        Class<?> clazz = manager.getClass();

        if(managers.contains(clazz)){
            return;
        }

        for(Manager m : managers){
            if(clazz.isAssignableFrom(m.getClass())){
                return;
            }
            managers.add(manager);
        }
    }

    public GameState getState(){
        return state;
    }

    public List<Manager> getManagers() {
        return managers;
    }

    public void setManagers(List<Manager> managers) {
        this.managers = managers;
    }

    public void setState(GameState state) {
        this.state = state;
    }

    public EngineGameListener getGameListener() {
        return gameListener;
    }

    public void setGameListener(EngineGameListener gameListener) {
        this.gameListener = gameListener;
    }
}
