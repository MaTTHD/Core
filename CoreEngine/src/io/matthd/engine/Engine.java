package io.matthd.engine;

import io.matthd.core.modular.Module;
import io.matthd.core.modular.ModuleMeta;
import io.matthd.engine.commands.WorldCommand;
import io.matthd.engine.events.EngineGameListener;
import io.matthd.engine.game.Game;

/**
 * Created by Matthew on 2016-01-02.
 */
@ModuleMeta(name = "CoreEngine",
        description = "Core's Game API to create games."
)
public class Engine extends Module {

    Game currentGame;

    private static Engine instance;

    @Override
    public void onModuleEnable() throws Exception {
        instance = this;
        getConfig().options().copyDefaults(true);
        saveConfig();
        getCommand("setup").setExecutor(new WorldCommand());
    }

    @Override
    public void onModuleDisable() throws Exception {
        instance = null;
    }

    public static Engine getInstance(){
        return instance;
    }

    public Game getCurrentGame() {
        return currentGame;
    }
}
