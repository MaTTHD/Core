package io.matthd.engine;

import io.matthd.engine.events.EngineTickEvent;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Matt on 05/01/2016.
 */
public class CountManager extends BukkitRunnable implements Manager {

    private int tick = 0;

    @Override
    public void run() {
        Bukkit.getServer().getPluginManager().callEvent(new EngineTickEvent(tick));
    }
}
