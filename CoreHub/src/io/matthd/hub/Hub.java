package io.matthd.hub;

import io.matthd.core.Core;
import io.matthd.core.Module;
import io.matthd.hub.listeners.*;
import io.matthd.hub.signs.SignManager;
import org.bukkit.Bukkit;

/**
 * Created by Matthew on 2015-12-31.
 */
public class Hub extends Module {


    private static Hub plugin;
    private SignManager signManager;

    @Override
    public void onFailureToDisable() {
        getLogger().severe("Hub module failed to disable!");
    }

    @Override
    public void onFailureToEnable() {
        getLogger().severe("Hub module could not load, shutting down!");
    }

    public void onModuleEnable() {
        plugin = this;
        signManager = new SignManager();
        Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        getConfig().options().copyDefaults(true);
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        saveDefaultConfig();
        getSignManager().loadSigns();

        System.out.println(Bukkit.getServer().getServerName());
        registerListener(new SignListener());
        registerListener(new SignClick());
        registerListener(new PlayerJoin());
        registerListener(new InteractEvent());
        registerListener(new InvClick());
        registerListener(new DamageEvent());
        registerListener(new FoodLevelChange());
        //pm.registerEvents(new BlockBreak(), this);
        registerListener(new PlayerQuit());
        //pm.registerEvents(new BlockPlace(), this);
        registerListener(new ItemDrop());
        registerListener(new MobSpwn());

        getSignManager().loadSigns();
        getSignManager().start();
    }

    public SignManager getSignManager(){
        return signManager;
    }

    public void onModuleDisable() {
        plugin = null;
    }

    public static Hub getPlugin() {
        return plugin;
    }
}
