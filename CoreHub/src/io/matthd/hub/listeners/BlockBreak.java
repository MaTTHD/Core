package io.matthd.hub.listeners;

import io.matthd.core.Core;
import io.matthd.core.backend.rank.Rank;
import io.matthd.hub.Hub;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;


/**
 * Created by Matt on 05/05/2015.
 */
public class BlockBreak implements Listener {

    private Core core = Core.getInstance();
    private Hub plugin = Hub.getPlugin();

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        if (core.getRankManager().getRank(e.getPlayer()) != Rank.OWNER && core.getRankManager().getRank(e.getPlayer()) != Rank.ADMIN) {
            e.setCancelled(true);
        }
        if (e.getBlock().getType() == Material.SIGN_POST || e.getBlock().getType() == Material.WALL_SIGN) {
            Sign sign = (Sign) e.getBlock().getState();

            if (plugin.getSignManager().getAllSigns().contains(sign) && !(e.getPlayer().isOp())) {
                e.setCancelled(true);
            } else {
                e.setCancelled(false);
            }
        }
    }
}
