package io.matthd.hub.listeners;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import io.matthd.core.Core;
import io.matthd.hub.Hub;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created by Matt on 03/05/2015.
 */
public class SignClick implements Listener {

    private Hub plugin = Hub.getPlugin();

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onClick(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getClickedBlock().getType() == Material.SIGN_POST || e.getClickedBlock().getType() == Material.WALL_SIGN) {
                Sign sign = (Sign) e.getClickedBlock().getState();
                //This is a game sign
                String type = ChatColor.stripColor(sign.getLine(1));
            	System.out.println(type);
                String serverToTP = plugin.getSignManager().getServerAvailible(type);
                if(serverToTP == null){
                	e.getPlayer().sendMessage(ChatColor.RED + "Could not find a open server!");
                	return;
                }
                System.out.println(serverToTP);
                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                out.writeUTF("Connect");
                out.writeUTF(serverToTP);
                e.getPlayer().sendPluginMessage(plugin, "BungeeCord", out.toByteArray());

            } else {
                return;
            }
        }
    }
}
