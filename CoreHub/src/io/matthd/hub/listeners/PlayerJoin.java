package io.matthd.hub.listeners;

import io.matthd.core.backend.player.CPlayer;
import io.matthd.core.backend.rank.Rank;
import io.matthd.core.backend.utils.Title;
import io.matthd.core.Core;
import io.matthd.core.backend.utils.ChatUtils;
import io.matthd.hub.util.ItemUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;


/**
 * Created by Matthew on 2015-05-10.
 */
public class PlayerJoin implements Listener {

    Location lobby = new Location(Bukkit.getWorld("Hub"), -29.095, 4.00000, -271.740, (float) -90.6, 0);

    private Core plugin = Core.getInstance();

    public void onJoin(PlayerJoinEvent e) {
        final Player player = e.getPlayer();
        if (plugin.getRankManager().getRank(player) == null) {
            new BukkitRunnable() {
                public void run() {
                    plugin.getRankManager().setRank(player, Rank.DEFAULT);
                }
            }.runTaskLater(plugin, 5L);
        }
        plugin.getPlayerManager().loadPlayer(player);

        CPlayer splayer = plugin.getPlayerManager().getPlayer(e.getPlayer());

        PlayerInventory pi = e.getPlayer().getInventory();
        pi.clear();
        ItemStack serverselector = ItemUtil.createItem(Material.COMPASS, ChatUtils.color("&a&lGAME SELECTOR"));
        ItemStack hubselector = ItemUtil.createItem(Material.BEDROCK, ChatUtils.color("&a&lHUB SELECTOR"));
        ItemStack cosmetics = ItemUtil.createItem(Material.CHEST, ChatUtils.color("&a&lCOSMETICS"));
        pi.setItem(4, serverselector);
        pi.setItem(2, cosmetics);
        pi.setItem(6, hubselector);
        pi.setHeldItemSlot(4);
        Title.sendTitle(player, 20, 20, 20, "&e&lWelcome to Survival Madness, ", " &a" + player.getName());
        player.setHealth(player.getMaxHealth());
        player.setFoodLevel(20);
        player.teleport(lobby);
        player.getInventory().setHeldItemSlot(4);
        player.sendMessage(ChatUtils.color("&6----------------------------------------"));
        player.sendMessage(ChatUtils.color("&aStore: &dstore.survivalmadness.net"));
        player.sendMessage(ChatUtils.color("&aOwners: &4LionMakerStudios &eand &4bmcc1234"));
        player.sendMessage(ChatUtils.color("&aDevelopers: &4bmcc1234 &eand &cTormatic"));
        player.sendMessage(ChatUtils.color("&6----------------------------------------"));
        if (splayer.getRank() == Rank.DEFAULT) {
            e.setJoinMessage(null);
        } else {
            e.setJoinMessage(ChatUtils.color(splayer.getRank().getColor() + splayer.getRankPrefix() + " " + ChatColor.RESET + splayer.getRank().getColor() + splayer.getCurrentName() + " &r&fhas joined the hub!"));
        }
    }
}
