package io.matthd.hub.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;


/**
 * Created by Shiv on 6/14/2015.
 */
public class PlayerQuit implements Listener {

    @EventHandler
    public void oCPlayerQuit(PlayerQuitEvent e) {
        e.setQuitMessage(null);
    }

}
