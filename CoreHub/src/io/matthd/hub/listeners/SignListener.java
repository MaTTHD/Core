package io.matthd.hub.listeners;



import io.matthd.core.Core;
import io.matthd.hub.Hub;
import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Matt on 03/05/2015.
 */
public class SignListener implements Listener {

	private Hub plugin = Hub.getPlugin();

	@EventHandler
	public void onSignPlace(final SignChangeEvent e) {
		if (!plugin.getSignManager().getAllSigns()
				.contains(e.getBlock().getState())) {

			plugin.getSignManager()
					.getInstance()
					.getRawLineData()
					.put((Sign) e.getBlock().getState(),
							ChatColor.stripColor(e.getLine(1)));
			String type = ChatColor.stripColor(e.getLine(0));

			if (type.equalsIgnoreCase("solo") || type.equalsIgnoreCase("team") || type.equalsIgnoreCase("Skywars")) {
				e.setLine(0, ChatColor.GREEN + ChatColor.BOLD.toString()
						+ "Skywars");
				e.setLine(1, ChatColor.GREEN + ChatColor.BOLD.toString() +type);
				e.setLine(2, "Players playing: "
						+ plugin.getSignManager().getInstance().getPlayersPlayingType(type));
				e.setLine(3, "Click to Join");
				System.out.println(e.getLine(2));
				new BukkitRunnable(){
					public void run(){
						plugin.getSignManager().getInstance().addSign(e.getBlock().getLocation());
						plugin.getSignManager().getInstance().updateSign((Sign) e.getBlock().getState());
					}
				}.runTaskLater(plugin, 3*20L);
			} else {
				return;
			}
		}
	}
}
