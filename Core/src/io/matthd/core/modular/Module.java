package io.matthd.core.modular;

import io.matthd.core.Core;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Matthew on 2015-12-04.
 */

public abstract class Module extends JavaPlugin {

    private ModuleMeta meta;

    public ModuleMeta getMeta() {
        return meta;
    }

    public void setMeta(ModuleMeta meta) {
        this.meta = meta;
    }

    @Override
    public final void onEnable() {
        try {
            if (!Core.getInstance().isEnabled()) {
                onFailureToEnable();
                return;
            }

            Core.getInstance().onModulePreEnable(this);
            getConfig().options().copyDefaults(true);
            saveConfig();
            meta = getClass().getAnnotation(ModuleMeta.class);
                if (meta == null) throw new IllegalStateException("You must annotate your class with the @" + ModuleMeta.class.getName() + " annotation!");
            onModuleEnable();
        } catch (Exception e) {
            onFailureToEnable();
        }
        logColorMessage("Module " + meta.name() + " was enabled!");
    }

    @Override
    public final void onDisable() {
        try {
            onModuleDisable();
            Core.getInstance().onModulePreDisable(this);
        } catch (Exception e) {
            onFailureToDisable();
        }
    }

    protected void onFailureToDisable() {}

    protected void onFailureToEnable(){}

    protected void onModuleEnable() throws Exception {};

    protected void onModuleDisable() throws Exception {};

    public final <T extends Listener> T registerListener(T listener) {
        getServer().getPluginManager().registerEvents(listener, this);
        return listener;
    }

    public void logColorMessage(String msg){
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&3[&f" + meta.name() + "&3] " + msg));
    }
}
