package io.matthd.core;

import io.matthd.core.backend.cmds.MultiplierCommand;
import io.matthd.core.backend.cmds.PixelCommand;
import io.matthd.core.backend.cmds.RankCommand;
import io.matthd.core.backend.cmds.ServerCommand;
import io.matthd.core.backend.listeners.*;
import io.matthd.core.backend.player.CorePlayerManager;
import io.matthd.core.backend.server.PreventionSet;
import io.matthd.core.backend.server.RedisManager;
import io.matthd.core.backend.server.ServerManager;
import io.matthd.core.backend.utils.MySQL;
import io.matthd.core.modular.Module;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Matthew on 2015-12-04.
 */
public class Core extends JavaPlugin {

    private static Core instance;

    private List<Module> modules = new ArrayList<>();

    private MySQL sql;
    private ServerManager sm;
    private CorePlayerManager pm;
    private RedisManager rd;

    private PreventionSet set;
    private int moduleCount = 0;


    private void init(){
        sql = new MySQL(instance, "localhost", "3306", "core", "root", "your_password");
        sm = new ServerManager();
        pm = new CorePlayerManager();
        rd = new RedisManager();
        set = new PreventionSet();
        sql.setup();
    }

    public void onModulePreEnable(Module module){
        this.modules.add(module);
        moduleCount++;
        System.out.println("Modules loaded: " + moduleCount);
    }

    public void onModulePreDisable(Module module){
        this.modules.add(module);
    }

    public void onEnable(){
        instance = this;
        init();
        getConfig().options().copyDefaults(true);
        saveConfig();

        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new PlayerChat(), this);
        pm.registerEvents(new FoodLevelChange(), this);
        pm.registerEvents(new PlayerCommandPreprocess(), this);
        pm.registerEvents(new PlayerJoin(), this);
        pm.registerEvents(new PlayerLogin(), this);
        pm.registerEvents(new PlayerQuit(), this);

        getCommand("setrank").setExecutor(new RankCommand());
        getCommand("multiplier").setExecutor(new MultiplierCommand());
        getCommand("coins").setExecutor(new PixelCommand());
        getCommand("leave").setExecutor(new ServerCommand());
    }

    public void onDisable(){
        instance = null;
    }

    public static Core getInstance(){
        return instance;
    }

    public MySQL getSql(){
        return sql;
    }

    public ServerManager getServerManager(){
        return sm;
    }

    public CorePlayerManager getPlayerManager(){
        return pm;
    }

    public RedisManager getRedisManager(){
        return rd;
    }

    public PreventionSet getSet(){
        return set;
    }
}
