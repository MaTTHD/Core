package io.matthd.core.gameapi.game;

import io.matthd.core.gameapi.Arcade;

import io.matthd.core.gameapi.kit.KitHandler;
import io.matthd.core.gameapi.player.ArcadePlayer;
import io.matthd.core.gameapi.team.Team;
import io.matthd.core.gameapi.timer.GameTimer;
import io.matthd.core.gameapi.util.LocationUtil;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Matthew on 2015-10-11.
 */
public class ArcadeHandler {

    private List<ArcadeGame> enabledGames = new ArrayList<>();
    private ArcadeGame lastGame;
    private ArcadeGame currentGame;
    private GameTimer timer;

    private static ArcadeHandler instance;

    public void init(){
        currentGame = enabledGames.get(0);
        lastGame = currentGame;
    }

    public static ArcadeHandler getInstance(){
        if(instance == null){
            instance = new ArcadeHandler();
        }
        return instance;
    }

    public ArcadeGame getCurrentGame(){
        if(lastGame == null){
            currentGame = enabledGames.get(0);
        }
        return currentGame;
    }

    public ArcadeGame getNextGame(){
        Random ran = new Random();
        int ranInt = ran.nextInt(enabledGames.size() - 1);
        if(enabledGames.get(ranInt) == lastGame){
            return getNextGame();
        }
        else {
            return enabledGames.get(ranInt);
        }
    }

    public void startGame(){
        KitHandler handler = Arcade.getInstance().getKitHandler();
        for(ArcadePlayer player : currentGame.getAllPlayers()){
            handler.getKit(player).giveKit(player);
        }

        if(currentGame.getInfo().isTeamGame()){
            for(Team t : currentGame.getTeams()){
                for(ArcadePlayer pl : t.getPlayers()){
                    pl.getPlayer().teleport(currentGame.getCurrentMap().getSpawns().get(currentGame.getTeams().indexOf(t)));
                }
            }
        }
        else {
            LocationUtil.distribute(currentGame.getAllPlayers(), currentGame.getCurrentMap().getSpawns());
            Arcade.getInstance().setState(GameState.INGAME);
        }
    }

    public void endRound(){
        for(ArcadePlayer player : currentGame.getAllPlayers()){
            player.getPlayer().teleport(currentGame.getCurrentMap().getLobby());
            player.getPlayer().getInventory().clear();
        }
        Arcade.getInstance().setState(GameState.LOBBY);
    }

    public void cycleGame(){
        int index = enabledGames.indexOf(currentGame);
        if(enabledGames.get(index + 1) == null){
            stopGame();
        }
        currentGame = enabledGames.get(index + 1);
        startGame();
    }

    public void stopGame(){
        //TODO display winners.
    }

    public GameTimer getTimer(){
        return timer;
    }

    public void setTimer(GameTimer timer){
        this.timer = timer;
    }

    public List<ArcadeGame> getEnabledGames() {
        return enabledGames;
    }

    public ArcadeGame getLastGame() {
        return lastGame;
    }

    public void setCurrentGame(ArcadeGame game){
        this.currentGame = game;
    }
}
