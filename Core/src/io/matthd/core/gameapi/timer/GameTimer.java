package io.matthd.core.gameapi.timer;

import io.matthd.core.gameapi.game.GameState;
import io.matthd.core.gameapi.Arcade;

import io.matthd.core.gameapi.events.ArcadeTickEvent;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Matthew on 2015-10-10.
 */
public class GameTimer extends BukkitRunnable {

    int lobbySeconds, gameSeconds;

    public GameTimer(int lobbySeconds, int gameSeconds){
        this.lobbySeconds = lobbySeconds;
        this.gameSeconds = gameSeconds;
    }

    @Override
    public void run() {
        GameState state = Arcade.getInstance().getState();
        if(state == GameState.LOBBY){
            Bukkit.getPluginManager().callEvent(new ArcadeTickEvent(state, lobbySeconds));
            lobbySeconds = (lobbySeconds-1);
        }
        else if(state == GameState.INGAME){
            Bukkit.getPluginManager().callEvent(new ArcadeTickEvent(state, gameSeconds));
            gameSeconds = (gameSeconds -1);
        }
    }

    public int getLobbySeconds() {
        return lobbySeconds;
    }

    public void setLobbySeconds(int lobbySeconds) {
        this.lobbySeconds = lobbySeconds;
    }

    public int getGameSeconds() {
        return gameSeconds;
    }

    public void setGameSeconds(int gameSeconds) {
        this.gameSeconds = gameSeconds;
    }
}
