package io.matthd.core.gameapi;


import io.matthd.core.modular.Module;
import io.matthd.core.gameapi.game.GameMap;
import io.matthd.core.gameapi.kit.Kit;
import io.matthd.core.gameapi.game.ArcadeGame;

/**
 * Created by Matthew on 2015-10-10.
 */
public abstract class GameCore extends Module {

    public abstract ArcadeGame setupGame();

    public abstract Kit[] registerKits();

    public abstract GameMap[] registerMaps();
}
