package io.matthd.core.backend.utils;

import org.bukkit.ChatColor;

/**
 * Created by Matthew on 2015-12-30.
 */
public class ChatUtils {

    public static String color(String input){
        return ChatColor.translateAlternateColorCodes('&', input);
    }

    public static String raw(String colorInput){
        return ChatColor.stripColor(colorInput);
    }
}
