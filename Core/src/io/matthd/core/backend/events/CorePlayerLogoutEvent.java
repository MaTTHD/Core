package io.matthd.core.backend.events;

import io.matthd.core.Core;
import io.matthd.core.backend.player.CorePlayer;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by Matthew on 2016-01-02.
 */
public class CorePlayerLogoutEvent extends Event implements Cancellable {

    CorePlayer player;

    public CorePlayerLogoutEvent(CorePlayer player){
        this.player = player;
    }

    public CorePlayer getPlayer(){
        return player;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public void setCancelled(boolean b) {

    }

    @Override
    public HandlerList getHandlers() {
        return null;
    }
}
