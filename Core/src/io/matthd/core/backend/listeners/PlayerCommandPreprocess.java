package io.matthd.core.backend.listeners;



import io.matthd.core.Core;
import io.matthd.core.backend.player.CorePlayer;
import io.matthd.core.backend.utils.ChatUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Created by Shiv on 6/14/2015.
 */
public class PlayerCommandPreprocess implements Listener {

    private Core plugin = Core.getInstance();

    /**
     * Stops /plugins and /help to avoid spammy annoying bukkit commands
     */
    @EventHandler
    public void oCPlayerCommand(PlayerCommandPreprocessEvent e) {
        final Player p = e.getPlayer();
        CorePlayer pl = plugin.getPlayerManager().getCorePlayer(p.getUniqueId());
        if (e.getMessage().startsWith("/plugins") || e.getMessage().startsWith("/plugins ") || e.getMessage().startsWith("/pl") || e.getMessage().startsWith("/pl ") || e.getMessage().startsWith("/bukkit:plugins") || e.getMessage().startsWith("/bukkit:plugins ")) {

            if (!plugin.getPlayerManager().isAuthorized("admin", pl)) {

                p.sendMessage(ChatColor.RED + "All you need to know is Core is enabled!");
                e.setCancelled(true);
                return;
            }
            e.setCancelled(false);
        }

        if (e.getMessage().startsWith("/help")  || e.getMessage().startsWith("/help ") || e.getMessage().startsWith("/bukkit:help") || e.getMessage().startsWith("/bukkit:help ")) {
            pl.getBukkitPlayer().sendMessage(ChatUtils.color("&eThis command is not available to public players &e&lyet&r&e."));
            e.setCancelled(true);
            // TODO MAKE MENU
        }

        if(e.getMessage().startsWith("/ban") || e.getMessage().startsWith("/ban ")){
            String[] args = e.getMessage().split(" ");
            // TODO ENTIRE CMD
        }
    }
}
