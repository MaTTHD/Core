package io.matthd.core.backend.listeners;

import io.matthd.core.Core;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

/**
 * Created by Matt on 29/05/2015.
 */
public class FoodLevelChange implements Listener {
    /**
     * Cancels food level change, as long as it is set in the permission set.
     * @param e
     */
    private Core plugin = Core.getInstance();

    @EventHandler
    public void change(FoodLevelChangeEvent e) {
        if (plugin.getSet().isFoodLoss()) {

        } else {
            e.setCancelled(true);
        }
    }
}
