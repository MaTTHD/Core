package io.matthd.core.backend.listeners;


import io.matthd.core.Core;
import io.matthd.core.backend.player.CorePlayer;
import io.matthd.core.backend.rank.Rank;
import io.matthd.core.backend.utils.ChatUtils;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by Matt on 29/05/2015.
 */
public class PlayerChat implements Listener {
    private Core plugin = Core.getInstance();

    /**
     * Simple Chat handler
     * @param e
     */

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onChat(AsyncPlayerChatEvent e) {
        if (plugin.getSet().isChatAllowed()) {
            CorePlayer pl = plugin.getPlayerManager().getCorePlayer(e.getPlayer().getUniqueId());

            String rawMessage = e.getMessage();
            String newMessage = rawMessage.replace("%", "%%");
            /**
             * Bukkit's weird chat formatting makes a single % as a place holder, so must change it to %%
             */

            if (pl.getGeneral().getRank() == Rank.NONE) {
                /**
                 * Different chat format for Default ranks.
                 */
                e.setFormat(ChatUtils.color(pl.getGeneral().getRank().getColor() + " " + pl.getName() + "&6: &f" + newMessage));
            } else {
                e.setFormat(ChatUtils.color(pl.getGeneral().getRankPrefix() + " " + ChatColor.RESET + pl.getGeneral().getRank().getColor() + pl.getName() + "&6: &f" + newMessage));
                return;
            }
        }
        else {
            e.setCancelled(true);
        }
    }
}
