package io.matthd.core.backend.listeners;


import io.matthd.core.Core;
import io.matthd.core.backend.events.CorePlayerLogoutEvent;
import io.matthd.core.backend.player.CorePlayer;
import io.matthd.core.backend.utils.ChatUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Matt on 03/05/2015.
 */
public class PlayerQuit implements Listener {

    private Core plugin = Core.getInstance();

    /**
     * Called when a player quits, to remove and or cache user data.
     * @param e
     */

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        CorePlayer player = plugin.getPlayerManager().getCorePlayer(e.getPlayer().getUniqueId());
        Bukkit.getServer().getPluginManager().callEvent(new CorePlayerLogoutEvent(player));
    }
}
