package io.matthd.core.backend.listeners;


import io.matthd.core.Core;
import io.matthd.core.backend.player.CorePlayer;
import io.matthd.core.backend.rank.Rank;
import io.matthd.core.backend.utils.ChatUtils;
import io.matthd.core.backend.utils.ScoreboardUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Matt on 03/05/2015.
 */
public class PlayerJoin implements Listener {

	private Core plugin = Core.getInstance();
	/**
	 * Join event listening for players joining and leaving.
	 * 
	 * @param e
	 */

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		final Player player = e.getPlayer();
		e.setJoinMessage(null);

		plugin.getPlayerManager().login(player);

		CorePlayer splayer = plugin.getPlayerManager().getCorePlayer(player.getUniqueId());

		plugin.getServerManager().addToPlayerCount(
				plugin.getServerManager().getServer(
						Bukkit.getServer().getServerName()));

		if (splayer.getGeneral().getRank() == Rank.NONE) {
			player.setPlayerListName(ChatUtils.color("&7" + player.getName()));
		} else {
			player.setPlayerListName(ChatUtils.color(
					splayer.getGeneral().getRankPrefix() + " " + ChatColor.RESET
					+ splayer.getGeneral().getRank().getColor() + splayer.getName()));
			// if name is more than 16+ characters, disable method.
		}
        if (plugin.getServerManager()
                .getHubs()
                .contains(
                        plugin.getServerManager().getServer(
                                Bukkit.getServerName()))) {
            ScoreboardUtils.setHubScoreboard(player);
        }
		else {
			
		}
	}
}
