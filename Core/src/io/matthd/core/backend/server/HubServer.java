package io.matthd.core.backend.server;

/**
 * Created by Matt on 21/06/2015.
 */
public class HubServer extends PlayableServer {

    /**
     * Simple constructor
     * @param name name of server
     */
    public HubServer(String name) {
        super(name);
    }
}
