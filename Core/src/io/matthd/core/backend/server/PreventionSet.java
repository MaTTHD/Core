package io.matthd.core.backend.server;

/**
 * Created by Matthew on 2015-12-31.
 */
public class PreventionSet {

    private boolean isFoodLoss = false;
    private boolean isPVP = false;
    private boolean isPVE = false;
    private boolean isChatAllowed = true;

    public PreventionSet(){

    }

    public PreventionSet(boolean isFoodLoss, boolean isPVP, boolean isPVE, boolean isCanTalk) {
        this.isFoodLoss = isFoodLoss;
        this.isPVP = isPVP;
        this.isPVE = isPVE;
        this.isChatAllowed = isCanTalk;
    }

    public boolean isFoodLoss() {
        return isFoodLoss;
    }

    public void setIsFoodLoss(boolean isFoodLoss) {
        this.isFoodLoss = isFoodLoss;
    }

    public boolean isPVP() {
        return isPVP;
    }

    public void setIsPVP(boolean isPVP) {
        this.isPVP = isPVP;
    }

    public boolean isPVE() {
        return isPVE;
    }

    public void setIsPVE(boolean isPVE) {
        this.isPVE = isPVE;
    }

    public boolean isChatAllowed() {
        return isChatAllowed;
    }

    public void setChatAllowed(boolean isCanTalk) {
        this.isChatAllowed = isCanTalk;
    }
}
