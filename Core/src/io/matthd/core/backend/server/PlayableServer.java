package io.matthd.core.backend.server;




import io.matthd.core.backend.player.CorePlayer;
import io.matthd.core.gameapi.game.GameState;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matt on 21/06/2015.
 */
public class PlayableServer {


    String name;
    List<CorePlayer> players;
    int onlineCount;
    GameState state;
    int multiplier;

    public PlayableServer(String name){
        this.name = name;
        this.players = new ArrayList<>();
        this.onlineCount = players.size();
        state = GameState.LOBBY;
    }

    /**
     * Gets name of a server
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name for the server
     * @param name name to set it too
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public List<CorePlayer> getPlayers() {
        return players;
    }

    /**
     * Sets list of online players
     * @param players list of players
     */
    public void setPlayers(List<CorePlayer> players) {
        this.players = players;
    }

    /**
     * gets the amount of online players
     * @return amount of players
     */
    public int getOnlineCount() {
        return onlineCount;
    }

    /**
     * sets the amount of online players
     * @param onlineCount amount of online players
     */
    public void setOnlineCount(int onlineCount) {
        this.onlineCount = onlineCount;
    }

    /**
     * gets the state of the server
     * @return state of the server
     */
    public GameState getState() {
        return state;
    }

    /**
     * sets the state of a server
     * @param state state to set
     */
    public void setState(GameState state) {
        this.state = state;
    }

    /**
     * gets the server multiplier
     * @return multiplier amount
     */
    public int getMultiplier() {
        return multiplier;
    }

    /**
     * sets multiplier of server
     * @param multiplier multiplier
     */
    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }
}
