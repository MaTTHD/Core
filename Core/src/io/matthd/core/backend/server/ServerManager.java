package io.matthd.core.backend.server;

import io.matthd.core.Core;
import io.matthd.core.gameapi.game.GameState;

import java.util.*;

/**
 * Created by Matt on 21/06/2015.
 */
public class ServerManager {

    List<HubServer> hubs = new ArrayList<>();
    List<PlayableServer> allServers = new ArrayList<>();

    private Core plugin = Core.getInstance();

    public void loadHubs() {
        for(String s : plugin.getRedisManager().getJedis().keys("server:*")){
            String newServer = s.replace("server:", "");
            if(newServer.contains("hub")){
                HubServer hubServer = new HubServer(newServer);
                hubs.add(hubServer);
            }
            return;
        }
    }

    public List<HubServer> getHubs(){
        return hubs;
    }


    public GameState getState(PlayableServer server){
        return GameState.valueOf(plugin.getRedisManager().getJedis().hget("server:" + server.getName(), "state"));
    }

    public void setState(PlayableServer server, GameState state){
        plugin.getRedisManager().getJedis().hset("server:" + server.getName(), "state", state.toString());
    }

    public void loadAllServers() {
        for (String s : plugin.getRedisManager().getJedis().keys("server:*")) {
            String newServer = s.replace("server:", "");
            PlayableServer server = new PlayableServer(newServer);
            allServers.add(server);
        }
    }

    public void addServer(String serverName) {
        if(serverName.contains("Hub") || serverName.contains("hub")){
            HubServer server = new HubServer(serverName);
            Map<String, String> data = new HashMap<>();
            data.put("name", serverName);
            data.put("state", server.getState().toString());
            data.put("maps", "world");
            data.put("online", Integer.toString(server.getOnlineCount()));
            data.put("multiplier", Integer.toString(0));
            allServers.add(server);
            plugin.getRedisManager().getJedis().hmset("server:" + serverName, data);
            plugin.getRedisManager().getJedis().save();
            hubs.add(server);
        }
        else {
            PlayableServer server = new PlayableServer(serverName);
            Map<String, String> data = new HashMap<>();
            data.put("name", serverName);
            data.put("state", server.getState().toString());
            data.put("maps", "world");
            data.put("online", Integer.toString(server.getOnlineCount()));
            data.put("multiplier", Integer.toString(0));
            data.put("type", "solo");
            allServers.add(server);
            plugin.getRedisManager().getJedis().hmset("server:" + serverName, data);
            plugin.getRedisManager().getJedis().save();
        }
    }

    public void addToPlayerCount(PlayableServer server){
        Integer amount = getOnlinePlayers(server);
        plugin.getRedisManager().getJedis().hset("server:" + server.getName(), "online", String.valueOf(amount + 1));
        System.out.println("Online count: " + getOnlinePlayers(server));
    }

    public int getOnlinePlayers(PlayableServer server){
        return Integer.parseInt(plugin.getRedisManager().getJedis().hget("server:" + server.getName(), "online"));
    }

    public void removePlayers(PlayableServer server){
        Integer amount = getOnlinePlayers(server);
        plugin.getRedisManager().getJedis().hset("server:" + server.getName(), "online", String.valueOf(amount - 1));
    }

    public PlayableServer getServer(String serverName) {
        for (PlayableServer server : allServers) {
            if (server.getName().equalsIgnoreCase(serverName)) {
                return server;
            }
        }
        return null;
    }

    public List<PlayableServer> getAllServers(){
        return allServers;
    }

    public List<PlayableServer> getServersFromName(String name){
        List<PlayableServer> servers = new ArrayList<>();

        for(PlayableServer server : allServers){
            if(server.getName().contains(name)){
                servers.add(server);
            }
            else {
                continue;
            }
        }
        return servers;
    }
    
    public void setType(PlayableServer server, String type){
    	plugin.getRedisManager().getJedis().hset("server:" + server.getName(), "type", type);
    }
    
    public String getType(PlayableServer server){
    	return plugin.getRedisManager().getJedis().hget("server:" + server.getName(), "type");
    }
    
    public int getAllOnlinePlayers(){
    	int x = 0;
    	for(PlayableServer server : allServers){
        	x+=Integer.parseInt(plugin.getRedisManager().getJedis().hget("server:" + server.getName(), "online"));
    	}
    	return x;
    }
    
    public String getMap(PlayableServer server){
    	return plugin.getRedisManager().getJedis().hget("server:" + server.getName(), "maps");
    }
    
    public boolean isServer(String server){
    	if(getServer(server) == null){
    		return false;
    	}
    	return true;
    }
    
    public void setMap(PlayableServer server, String worldName){
    	plugin.getRedisManager().getJedis().hset("server:" + server.getName(), "maps", worldName);
    }
}
