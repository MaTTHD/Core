package io.matthd.core.backend.cmds;

import io.matthd.core.Core;
import io.matthd.core.backend.player.CorePlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Matt on 2015-07-14.
 */
public class PixelCommand implements CommandExecutor {
    private Core plugin = Core.getInstance();

    /**
     *
     *
     * Basic economy command, allows admins to take, set, and add currency to a players cache.
     *
     * @param sender Returns the sender of the command
     *
     * @param cmd Returns the command being executed
     *
     * @param s Returns the command in String form
     *
     * @param args Returns the arguments passed, or done within the command
     * @return
     *
     *
     **/



    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(cmd.getName().equalsIgnoreCase("coins")){
            ConsoleCommandSender con = Bukkit.getServer().getConsoleSender();
            if(!(sender instanceof ConsoleCommandSender || !(plugin.getPlayerManager().isAuthorized("admin", plugin.getPlayerManager().getCorePlayer(((Player)sender).getUniqueId()))))){
                return true;
            }
            if(args.length != 3){
                sender.sendMessage(ChatColor.RED + "Usage: /coins [add,take,set] [player] [amount]");
                return true;
            }

            Player pl = Bukkit.getPlayer(args[1]);

            if(pl == null){
                sender.sendMessage(ChatColor.RED + "Player is not online!");
                return true;
            }

            try {
                int amount = Integer.valueOf(args[3]);

                if(args[0].equalsIgnoreCase("add")){

                    int newAmount = (plugin.getPlayerManager().getCorePlayer(pl.getUniqueId()).getGeneral().getCoins() + amount);
                    plugin.getPlayerManager().getCorePlayer(pl.getUniqueId()).getGeneral().setCoins(newAmount);
                    sender.sendMessage(ChatColor.AQUA + "Added " + ChatColor.YELLOW + amount + ChatColor.AQUA + " coins to " + ChatColor.YELLOW + pl.getName() + ChatColor.AQUA + "'s account!");
                    return true;
                }
                else if(args[0].equalsIgnoreCase("take")){

                    int newAmount = (plugin.getPlayerManager().getCorePlayer(pl.getUniqueId()).getGeneral().getCoins() - amount);
                    plugin.getPlayerManager().getCorePlayer(pl.getUniqueId()).getGeneral().setCoins(newAmount);

                    sender.sendMessage(ChatColor.AQUA + "Took " + ChatColor.YELLOW + amount + ChatColor.AQUA + " coins from " + ChatColor.YELLOW + pl.getName() + ChatColor.AQUA + "'s account!");
                    return true;
                }
            } catch(NumberFormatException e){
                e.printStackTrace();
            }
        }
        return false;
    }
}
