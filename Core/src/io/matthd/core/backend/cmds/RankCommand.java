package io.matthd.core.backend.cmds;


import io.matthd.core.Core;
import io.matthd.core.backend.rank.Rank;
import io.matthd.core.backend.utils.ChatUtils;
import io.matthd.core.backend.utils.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

/**
 * Created by Matt on 29/05/2015.
 */
public class RankCommand implements CommandExecutor {
	
	private Core plugin = Core.getInstance();
	

    /**
     *
     *
     * Basic rank command, allows admins to change the rank of the specified player.
     *
     * @param sender Returns the sender of the command
     *
     * @param cmd Returns the command being executed
     *
     * @param s Returns the command in String form
     *
     * @param args Returns the arguments passed, or done within the command
     * @return
     *
     *
     **/

    public boolean onCommand(CommandSender sender, Command cmd, String s, final String[] args) {
        if (cmd.getName().equalsIgnoreCase("setrank")) {
            if (sender instanceof Player) {
                final Player pl = (Player) sender;
                if (!(plugin.getPlayerManager().isAuthorized("admin", plugin.getPlayerManager().getCorePlayer(pl.getUniqueId())))) {
                    pl.sendMessage(ChatColor.RED + "You do not have permission!");
                    return true;
                } else {
                    if (args.length != 2) {
                        pl.sendMessage(ChatColor.RED + "Improper usage: /setrank [player] [rank]");
                    } else {
                        final Rank rank = Rank.valueOf(args[1].toUpperCase());
                        if (rank == null) {
                            pl.sendMessage(ChatColor.RED + "No rank with that name!");
                            return true;
                        } else {
                            Player target = Bukkit.getPlayer(args[0]);
                            if (target == null) {
                                new BukkitRunnable() {
                                    public void run() {
                                        try {
                                            UUID uuid = UUIDFetcher.getUUIDOf(args[0]);
                                            plugin.getPlayerManager().getCorePlayer(uuid).getGeneral().setRank(rank);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }.runTaskAsynchronously(Core.getInstance());
                                pl.sendMessage(ChatUtils.color("&e" + args[0] + " &cis currently not online, but their rank has been updated."));
                                return true;
                            }
                            else {
                                plugin.getPlayerManager().getCorePlayer(target.getUniqueId()).getGeneral().setRank(rank);
                                pl.sendMessage(ChatUtils.color("&a" + args[0] + "&e's rank has been set to " + rank.getColor() + rank.getName()));
                                return true;
                            }
                        }
                    }
                }
            }
            else {
                if (args.length != 2) {
                    sender.sendMessage(ChatColor.RED + "/setrank playername [rank]");
                } else {
                    final Rank rank = Rank.valueOf(args[1].toUpperCase());
                    if (rank == null) {
                        sender.sendMessage(ChatColor.RED + "No rank with that name!");
                        return true;
                    } else {
                        Player target = Bukkit.getPlayer(args[0]);
                        if (target == null) {

                            new BukkitRunnable() {
                                public void run() {
                                    try {

                                        UUID uuid = UUIDFetcher.getUUIDOf(args[0]);
                                        plugin.getPlayerManager().getCorePlayer(uuid).getGeneral().setRank(rank);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.runTaskAsynchronously(Core.getInstance());
                            sender.sendMessage(ChatColor.GREEN + "Cached player: " + args[0] + "'s rank as they are not online!");
                            return true;
                        }
                        else {
                            plugin.getPlayerManager().getCorePlayer(target.getUniqueId()).getGeneral().setRank(rank);
                            sender.sendMessage(ChatColor.GREEN + "set player: " + args[0] + "'s rank to: " + rank.name() + "!");
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
