package io.matthd.core.backend.player;


import io.matthd.core.Core;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Created by Matt on 02/06/2015.
 */
public class CorePlayer {

    private Core core = Core.getInstance();

    private PlayerGeneral general;
    private String name;
    private String server;
    private Player bukkitPlayer;

    public CorePlayer(Player bukkitPlayer) {
        this.bukkitPlayer = bukkitPlayer;
        general = PlayerGeneral.get(bukkitPlayer.getUniqueId());
        name = bukkitPlayer.getName();
        server = Bukkit.getServerName();
    }

    public Player getBukkitPlayer() {
        return bukkitPlayer;
    }

    public void setBukkitPlayer(Player bukkitPlayer) {
        this.bukkitPlayer = bukkitPlayer;
    }

    public PlayerGeneral getGeneral() {
        return general;
    }

    public void setGeneral(PlayerGeneral general) {
        this.general = general;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public void sendMessage(String msg){
        bukkitPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
    }

    public void teleport(Location location){
        bukkitPlayer.teleport(location);
    }
}
