package io.matthd.core.backend.player;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.matthd.core.Core;
import io.matthd.core.backend.rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Matt on 02/06/2015.
 */
public class CorePlayerManager {

   private List<CorePlayer> players = new ArrayList<>();

    public void login(Player player) {
        players.add(new CorePlayer(player));
    }

    public void logout(CorePlayer player){
        if(players.contains(player)){
            players.remove(player);
        }
    }

    public CorePlayer getCorePlayer(UUID uuid){
        for(CorePlayer player : players){
            if(player.getGeneral().getUUID().toString().equalsIgnoreCase(uuid.toString())){
                return player;
            }
        }
        return null;
    }

    public boolean isAuthorized(String auth, CorePlayer player){
        if(auth.equalsIgnoreCase("owner")){
            return player.getGeneral().getRank() == Rank.OWNER;
        }
        if(auth.equalsIgnoreCase("admin")){
            return player.getGeneral().getRank() == Rank.ADMIN || player.getGeneral().getRank() == Rank.OWNER;
        }
        if(auth.equalsIgnoreCase("mod")){
            return player.getGeneral().getRank() == Rank.MOD || player.getGeneral().getRank() == Rank.ADMIN || player.getGeneral().getRank() == Rank.OWNER;
        }
        if(auth.equalsIgnoreCase("none")){
            return true;
        }
        return true;
    }
}