package io.matthd.core.backend.player;

import io.matthd.core.Core;
import io.matthd.core.backend.rank.Rank;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Created by Matthew on 2016-01-02.
 */
public class PlayerGeneral {

    private final UUID uuid;
    private Rank rank;
    private int coins;
    private String wallPost;

    public PlayerGeneral(UUID uuid, Rank rank, int coins, String wallPost) {
        this.uuid = uuid;
        this.rank = rank;
        this.coins = coins;
        this.wallPost = wallPost;
    }

    public static PlayerGeneral get(UUID uuid) {
        PreparedStatement pst = null;

        try (Connection con = Core.getInstance().getSql().openConnection()){

            pst = con.prepareStatement("SELECT * FROM General WHERE UUID=?");
            pst.setString(1, uuid.toString());

            ResultSet set = pst.executeQuery();
            if(set.next()){
                Rank rank = Rank.valueOf(set.getString("Rank"));
                int coins = set.getInt("Coins");
                String wallPost = set.getString("Wall");
                return new PlayerGeneral(uuid, rank, coins, wallPost);
            }
            else {
                PreparedStatement insert = con.prepareStatement("INSERT INTO General VALUES(?,?,?,?)");

                Rank rank = Rank.NONE;
                int coins = 0;
                String wall = "No wall post! Update it with /wall";

                insert.setString(1, uuid.toString());
                insert.setString(2, rank.toString());
                insert.setInt(3, coins);
                insert.setString(4, wall);
                insert.execute();
                return new PlayerGeneral(uuid, rank, coins, wall);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setRank(Rank rank){
        try (Connection con = Core.getInstance().getSql().openConnection()) {
            PreparedStatement pst = con.prepareStatement("UPDATE General SET Rank=? WHERE UUID=?");
            pst.setString(1, rank.toString());
            pst.setString(2, uuid.toString());
            pst.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        this.rank = rank;
    }

    public void setCoins(int coins){
        try (Connection con = Core.getInstance().getSql().openConnection()) {
            PreparedStatement pst = con.prepareStatement("UPDATE General SET Coins=? WHERE UUID=?");
            pst.setInt(1, coins);
            pst.setString(2, uuid.toString());
            pst.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        this.coins = coins;
    }

    public void setWallPost(String wallPost) {
        try (Connection con = Core.getInstance().getSql().openConnection()) {
            PreparedStatement pst = con.prepareStatement("UPDATE General SET Wall=? WHERE UUID=?");
            pst.setString(1, wallPost);
            pst.setString(2, uuid.toString());
            pst.executeUpdate();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        this.wallPost = wallPost;
    }

    public int getCoins() {
        return coins;
    }

    public UUID getUUID() {
        return uuid;
    }

    public Rank getRank() {
        return rank;
    }

    public String getWallPost() {
        return wallPost;
    }


    public String getRankPrefix(){
        return getRank().getColor() + getRank().getName();
    }
}
