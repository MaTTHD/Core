package io.matthd.core.backend.rank;

import org.bukkit.ChatColor;



/**
 * Created by Matt on 21/04/2015.
 */
public enum Rank {

    NONE("", ChatColor.GRAY),
    MOD("Mod", ChatColor.DARK_GREEN),
    VIP("Vip", ChatColor.YELLOW),
    PLUS("Plus", ChatColor.AQUA),
    ADMIN("Admin", ChatColor.RED), 
    OWNER("Owner", ChatColor.DARK_RED);
    

    String name;
    ChatColor color;
    ChatColor bold;

    /**
     * Rank Enum
     * @param name name for the rank
     * @param color main color
     */
    Rank(String name, ChatColor color){
        this.name = name;
        this.bold = ChatColor.BOLD;
        this.color = color;
    }

    /**
     * Returns the main color
     * @return Returns ChatColor
     */
    public ChatColor getColor() {
        return color;
    }

    /**
     * Returns the name of the rank
     * @return Returns the string value for the name.
     */
    public String getName() {
        return name;
    }
}
